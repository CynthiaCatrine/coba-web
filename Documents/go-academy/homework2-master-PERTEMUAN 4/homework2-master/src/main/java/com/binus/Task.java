package com.binus;

public class Task {
    private int taskID;
    private String taskName;
    private String taskStatus;

    public Task(int taskID, String taskName, String taskStatus){
        this.taskID = taskID;
        this.taskName = taskName;
        this.taskStatus = taskStatus;
    }

    void setTaskStatus(){
        this.taskStatus = "DONE";
    }

    String getTaskStatus(){
        return this.taskStatus;
    }

    String getTaskList(){
        return this.taskID + ". " + this.taskName + " " + "[" + this.taskStatus + "]";
    }
}
