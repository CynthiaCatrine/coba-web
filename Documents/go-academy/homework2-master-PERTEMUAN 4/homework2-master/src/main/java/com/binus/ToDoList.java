package com.binus;

import java.util.*;

public class ToDoList {
    private ArrayList<Task> toDoList;

    public void addToDo(Task task){
        if(toDoList==null){
            this.toDoList = new ArrayList<>();
        }
        toDoList.add(task);
    }

    public String getToDoByIndex(int index){
        Task selected = toDoList.get(index);

        return selected.getTaskList();
    }

    public String getToDoList() {
        StringBuilder todoList = new StringBuilder();
        for (int i = 0; i < toDoList.size(); i++) {
            todoList.append(getToDoByIndex(i));
            if (i < toDoList.size() - 1) {
                todoList.append("\n");
            }
        }
        return todoList.toString();
    }

    public String updateToDoStatus(int index) {
        try {
            if (this.toDoList.get(index - 1).getTaskStatus().equals("NOT DONE")) {
                this.toDoList.get(index - 1).setTaskStatus();
                return "Task Status Updated";
            }
            else {
                return "Task Already Updated";
            }
        }
        catch (IndexOutOfBoundsException e) {
            return "Task Not Found";
        }
    }
}
