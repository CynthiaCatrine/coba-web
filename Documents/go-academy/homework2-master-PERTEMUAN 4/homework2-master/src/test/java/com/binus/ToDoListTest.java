package com.binus;

import org.junit.Test;

import static org.junit.Assert.*;

public class ToDoListTest {
    @Test
    public void testAddToDoByIndex(){
        String expectedTask = "1. Belajar [NOT DONE]";

        Task task1 = new Task(1, "Belajar", "NOT DONE");
        ToDoList myToDoList = new ToDoList();
        myToDoList.addToDo(task1);

        assertEquals(expectedTask, myToDoList.getToDoByIndex(0));
    }

    @Test
    public void testAddToDoList(){
        String expectedTask = "1. Ngoding [NOT DONE]\n2. Mandi [DONE]\n3. Makan [NOT DONE]";

        Task task1 = new Task(1, "Ngoding", "NOT DONE");
        Task task2 = new Task(2, "Mandi", "DONE");
        Task task3 = new Task(3, "Makan", "NOT DONE");
        ToDoList toDoListMany = new ToDoList();
        toDoListMany.addToDo(task1);
        toDoListMany.addToDo(task2);
        toDoListMany.addToDo(task3);

        assertEquals(expectedTask, toDoListMany.getToDoList());
    }

    @Test
    public void testUpdateTask(){
        String expected = "1. Ngoding [DONE]";

        Task task = new Task(1, "Ngoding", "NOT DONE");
        ToDoList todoList = new ToDoList();
        todoList.addToDo(task);
        todoList.updateToDoStatus(1);

        assertEquals(expected, task.getTaskList());
    }

    @Test
    public void testAlreadyUpdated(){
        String expected = "Task Already Updated";

        Task task = new Task(1, "Ngoding", "DONE");
        ToDoList todoList = new ToDoList();
        todoList.addToDo(task);

        assertEquals(expected, todoList.updateToDoStatus(1));
    }

    @Test
    public void testTaskNotFound(){
        String expected = "Task Not Found";
        Task task = new Task(1, "Ngoding", "DONE");
        ToDoList todoList = new ToDoList();
        todoList.addToDo(task);
        assertEquals(expected, todoList.updateToDoStatus(0));
    }
}